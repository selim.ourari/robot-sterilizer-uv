

# Robot Sterilizer UV

This repository, maintained by Selim Ouirari, contains the code and resources for an autonomous mobile robot project aimed at sterilization using ultraviolet (UV) light. The project was initiated during the COVID-19 pandemic as a response to the need for effective sterilization methods and developped with Mr Sadok Guesmi. The robot is designed to navigate autonomously in a given environment and sterilize surfaces using UV light.

The project is based on the Robot Operating System (ROS) Noetic and uses Simultaneous Localization and Mapping (SLAM) for navigation. The robot and its environment are simulated using Gazebo.

## Running the Simulation

To run the project in Gazebo simulation, follow these steps:

1. Ensure that you have ROS Noetic and Gazebo installed on your system. If not, you can follow the official ROS Noetic installation guide for your specific operating system [here](http://wiki.ros.org/noetic/Installation).

2. Clone this repository to your local system using the following command:

```bash
git clone https://gitlab.com/selim.ourari/robot-sterilizer-uv.git
```

3. Navigate to the `RobotUV` directory:

```bash
cd robot-sterilizer-uv/RobotUV
```

4. Build the project using catkin:

```bash
catkin_make
```

5. Source the setup file:

```bash
source devel/setup.bash
```

6. Launch the simulation:

```bash
roslaunch [your_launch_file.launch]
```

Replace `[your_launch_file.launch]` with the name of your launch file.

## Project Structure

The main code for the robot is located in the `RobotUV` directory. This directory contains several subdirectories:

- `config`: Contains configuration files.
- `launch`: Contains ROS launch files, which are used to start up nodes.
- `meshes`: Contains 3D models used in the robot description.
- `param`: Contains parameter files for configuring ROS nodes.
- `urdf`: Contains URDF (Unified Robot Description Format) files, which are used to describe the robot's physical layout.
- `worlds`: Contains files describing the simulation environment for the robot.

## Contributing

Contributions to this project are welcome. If you have suggestions or improvements, feel free to open an issue or submit a merge request.

## License

The code in this repository is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. You may not use this file except in compliance with the License. You may obtain a copy of the License at [here](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).

## Contact

If you have any questions or need further clarification, feel free to reach out to Selim Ouirari.

---

Please replace `[your_launch_file.launch]` with the actual name of your launch file. Also, note that this is a basic draft and you may need to add more details based on the actual content of the project.